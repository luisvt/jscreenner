/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.luisvargastijerino.jscreenner;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.prefs.Preferences;

/**
 *
 * @author luis
 */
public class RecordPreferences {

    private static final Preferences preferences = Preferences.userNodeForPackage(MainWindow.class);

    public static Rectangle getCaptureArea() {
        return new Rectangle(
                preferences.getInt("captureArea.x", 0),
                preferences.getInt("captureArea.y", 0),
                preferences.getInt("captureArea.width", 680),
                preferences.getInt("captureArea.height", 460)
        );
    }

    public static void setCaptureArea(Rectangle captureArea) {
        preferences.putInt("captureArea.x", captureArea.x);
        preferences.putInt("captureArea.y", captureArea.y);
        preferences.putInt("captureArea.width", captureArea.width);
        preferences.putInt("captureArea.height", captureArea.height);
    }

    public static int getFps() {
        return preferences.getInt("fps", 15);
    }

    public static void setFps(int fps) {
        preferences.putInt("fps", fps);
    }
    
    public static int getScreensOffset() {
        return preferences.getInt("screensOffset", 0);
    }
    
    public static void setScreensOffset(int screensOffset) {
        preferences.putInt("screensOffset", screensOffset);
    }
}
