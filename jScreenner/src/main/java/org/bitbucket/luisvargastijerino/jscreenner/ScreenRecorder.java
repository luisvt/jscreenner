package org.bitbucket.luisvargastijerino.jscreenner;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.concurrent.TimeUnit;

import com.xuggle.mediatool.ToolFactory;
import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.xuggler.IRational;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.SwingUtilities;

/**
 * Hello world!
 *
 */
public class ScreenRecorder extends Thread {

    private final IRational fps;
    private boolean finished = false;
    private boolean paused = false;
    public ScreenRecorder() {
        this.fps = IRational.make(RecordPreferences.getFps(), 1);
    }

    public void stopRecording() {
        finished = true;
        System.out.println("screen recorder finished");
    }

    public void pauseRecording() {
        paused = true;
    }
    
    public void resumeRecording() {
        paused = false;
    }
    
    /**
     * Takes a screen shot of your entire screen and writes it to output.flv
     *
     * @param args
     */
    @Override
    public void run() {
        try {
            final String outFile = "output.mp4";
            // This is the robot for taking a snapshot of the
            // screen.  It's part of Java AWT
            Robot robot = new Robot();
            
            Rectangle captureArea = RecordPreferences.getCaptureArea();
            int screensOffset = RecordPreferences.getScreensOffset();
            
            captureArea.y -= screensOffset;

            // First, let's make a IMediaWriter to write the file.
            IMediaWriter writer = ToolFactory.makeWriter(outFile);

            // We tell it we're going to add one video stream, with id 0,
            // at position 0, and that it will have a fixed frame rate of
            // FRAME_RATE.
            writer.addVideoStream(
                    0, 
                    0,
                    fps,
                    captureArea.width, 
                    captureArea.height);

            // Now, we're going to loop
            long startTime = System.nanoTime();

            URL mouseUrl = getClass().getResource("/images/Mouse_pointer_or_cursor.png");
            Image cursor = ImageIO.read(mouseUrl);

            while (!finished) {
                if(!paused) {
                    // take the screen shot
                    System.out.println("captureArea: " + captureArea);
                    BufferedImage screen = robot.createScreenCapture(captureArea);

                    Point mouseLocation = MouseInfo.getPointerInfo().getLocation();
                    mouseLocation.x -= captureArea.x;
                    mouseLocation.y -= captureArea.y + screensOffset;
                    System.out.println("mouse position: " + mouseLocation.x + ", " + mouseLocation.y);
                    
                    //Draw the cursor at the pointer position in the current screenshot
                    Graphics2D graphics2D = screen.createGraphics();
                    graphics2D.drawImage(cursor, mouseLocation.x, mouseLocation.y, 16, 16, null); // cursor.gif is 16x16 size.

                    // convert to the right image type
                    BufferedImage bgrScreen = convertToType(screen, BufferedImage.TYPE_3BYTE_BGR);

                    // encode the image
                    writer.encodeVideo(0, bgrScreen,
                            System.nanoTime() - startTime, TimeUnit.NANOSECONDS);

                    // sleep for framerate milliseconds
                    Thread.sleep((long) (1000 / fps.getDouble()));
                } else {
                    Thread.sleep((long) (500));
                    System.out.println("screen recorder paused");
                }
            }
            // Finally we tell the writer to close and write the trailer if
            // needed
            writer.close();
        } catch (Throwable e) {
            System.err.println("an error occurred: " + e.getMessage());
        }
    }

    /**
     * Convert a {@link BufferedImage} of any type, to {@link BufferedImage} of
     * a specified type. If the source image is the same type as the target
     * type, then original image is returned, otherwise new image of the correct
     * type is created and the content of the source image is copied into the
     * new image.
     *
     * @param sourceImage the image to be converted
     * @param targetType the desired BufferedImage type
     *
     * @return a BufferedImage of the specifed target type.
     *
     * @see BufferedImage
     */
    private BufferedImage convertToType(BufferedImage sourceImage,
            int targetType) {
        BufferedImage image;

        // if the source image is already the target type, return the source image
        if (sourceImage.getType() == targetType) {
            image = sourceImage;
        } // otherwise create a new image of the target type and draw the new
        // image
        else {
            image = new BufferedImage(sourceImage.getWidth(),
                    sourceImage.getHeight(), targetType);
            image.getGraphics().drawImage(sourceImage, 0, 0, null);
        }

        return image;
    }
}
